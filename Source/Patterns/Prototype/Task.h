// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CanClone.h"
#include "Task.generated.h"

/**
 * @brief A base class for all AI tasks.
 * @details Shall represent a short action as a part of same greather plan.
 */
UCLASS()
class PATTERNS_API UTask : public UObject, public ICanClone
{
	GENERATED_BODY()
public:
	/** BEGIN: ICanClone interface implementation */
	virtual UObject* Clone() override;
	/** END: ICanClone interface implementation */

	/** Return Duraction */
	float GetDuration() const
	{
		return Duraction;
	}

	/** Return IsRepeatable */
	bool IsRepeatable() const
	{
		return bIsRepeatable;
	}
	
private:
	/** @brief How long can completing this task take.
	 *	@details When set via BP editor is clumped to ten minutes. The sharter task is, the better.
	 */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Task",
		meta = (AllowPrivateAccess = "true", ClampMin = 0, ClampMax = 600, UIMin = 0, UIMax = 600))
		float Duraction;

	/** Can this task be restarted once done. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Task", meta = (AllowPrivateAccess = "true"))
		bool bIsRepeatable = true;
};

inline UObject* UTask::Clone()
{
	if(auto Copy = NewObject<UTask>(GetOuter()))
	{
		Copy->Duraction = GetDuration();
		Copy->bIsRepeatable = IsRepeatable();
		return Copy;
	}

	return nullptr;
}
