// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Patterns/AbstarctFactory/LootStructures.h"
#include "Patterns/AbstarctFactory/Loot/Elvens/ElvenLoot.h"
#include "Patterns/AbstarctFactory/Loot/Necro/NecroLoot.h"
#include "Forge.generated.h"

class ALoot;

/**
 * Abstract actor for creating things
 */
UCLASS()
class PATTERNS_API AForge : public AActor
{
	GENERATED_BODY()

public:
	/** Creating armor function */
	UFUNCTION(BlueprintPure)
		virtual ALoot* ForgeArmor() PURE_VIRTUAL(AForge::ForgeArmor, return nullptr; );

	/** Creating weapon function */
	UFUNCTION(BlueprintPure)
		virtual ALoot* ForgeWeapon() PURE_VIRTUAL(AForge::ForgeWeapon, return nullptr; );

protected:
	/** Return info for specific class
	 * @return Info Structure for class
	 */
	FBaseInfo* GetInfoForItem(UClass* ItemClass) PURE_VIRTUAL(AForge::GetInfoForItem, return nullptr; ); // Abstract function
};

/**
 * Actor creating things for the elves
 */
UCLASS()
class PATTERNS_API AElvenForge : public AForge
{
	GENERATED_BODY()

public:
	virtual ALoot* ForgeArmor() override;
	virtual ALoot* ForgeWeapon() override;
};

inline ALoot* AElvenForge::ForgeArmor()
{
	//Create elven armor item
	auto ItemArmor = GetWorld()->SpawnActor<AElvenArmor>(AElvenArmor::StaticClass());
	if (ItemArmor)
	{
		ItemArmor->SetupWithInfo(GetInfoForItem(AElvenArmor::StaticClass()));
	}
	return ItemArmor;
}

inline ALoot* AElvenForge::ForgeWeapon()
{
	//Create elven armor item
	auto ItemWeapon = GetWorld()->SpawnActor<AElvenWeapon>(AElvenWeapon::StaticClass());
	if (ItemWeapon)
	{
		ItemWeapon->SetupWithInfo(GetInfoForItem(AElvenWeapon::StaticClass()));
	}
	return ItemWeapon;
}

/**
 * Actor creating things for the necro
 */
UCLASS()
class PATTERNS_API ANecroForge : public AForge
{
	GENERATED_BODY()

public:
	virtual ALoot* ForgeArmor() override;
	virtual ALoot* ForgeWeapon() override;
};

inline ALoot* ANecroForge::ForgeArmor()
{
	//Create necro armor item
	auto ItemArmor = GetWorld()->SpawnActor<ANecroArmor>(ANecroArmor::StaticClass());
	if (ItemArmor)
	{
		ItemArmor->SetupWithInfo(GetInfoForItem(ANecroArmor::StaticClass()));
	}
	return ItemArmor;
}

inline ALoot* ANecroForge::ForgeWeapon()
{
	//Create necro weapon item
	auto ItemWeapon = GetWorld()->SpawnActor<ANecroWeapon>(ANecroWeapon::StaticClass());
	if (ItemWeapon)
	{
		ItemWeapon->SetupWithInfo(GetInfoForItem(ANecroWeapon::StaticClass()));
	}
	return ItemWeapon;
}