// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Patterns/Builder/CanGenerateCity.h"
#include "GenerationDirector.generated.h"

/**
 * A director that know how to create different type of citties
 */
UCLASS()
class PATTERNS_API UGenerationDirector : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	/** Step by step generaters elven city */
	static void GenerateElvenCity(ICanGenerateCity* Generator);

	/** Step by step generaters necromantic city */
	static void GenerateNecroCity(ICanGenerateCity* Generator);
};

inline void UGenerationDirector::GenerateElvenCity(ICanGenerateCity* Generator)
{
	if(Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateCastle();
		Generator->GenerateHourse();
	}
}

inline void UGenerationDirector::GenerateNecroCity(ICanGenerateCity* Generator)
{
	if(Generator)
	{
		Generator->GenerateGrounds();
		Generator->GenerateHourse();
	}
}